#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <cuda_profiler_api.h>
#include <cuda.h>
#include <assert.h>
using namespace std;
#define NUMBINS 3
#define BINSCAPACITY 14
#define GOAL 17
#define ENABLE_PRINT false
enum status {
    DEFAULT,
    ONGOING,
    FAILURE,
    SUCCESS,
    CANCELLED
};
enum pos {
    HIGH,
    LOW,
    NONE,
};
typedef struct Weights {
    char array[BINSCAPACITY];

    __host__ __device__ Weights(void) {
        for (int i = 0; i < BINSCAPACITY; i++) array[i] = 0;
    }
    __host__ __device__ Weights(const Weights &original) {
        for (int i = 0; i < BINSCAPACITY; i++) array[i] = original.array[i];
    }
    __host__ __device__ inline char& operator[](int i) {
        return array[i];
    }
    __host__ __device__ inline char operator[](int i) const {
        return array[i];
    }
    __host__ __device__ bool operator==(const Weights &p) const {
        bool ret = true;
        for (int i = 0; i < BINSCAPACITY; i++) ret &= array[i] == p.array[i];
        return ret;
    }
} Weights;

typedef struct Node {
    char bins[NUMBINS]; //the bins
    struct Weights weights; //the weight array
    char minWeight;
    char maxWeight;
    char nextPos;
    enum pos failed;

    __host__ __device__ Node (const Node &original) {
        for (int i = 0; i < NUMBINS; i++) bins[i] = original.bins[i];
        weights = {};
        for (int i = 0; i < BINSCAPACITY; i++) weights.array[i] = original.weights.array[i];
        minWeight = original.minWeight;
        maxWeight = original.maxWeight;
        nextPos = original.nextPos;
        failed = original.failed;
    }
    __host__ __device__ Node(const char newbins[NUMBINS], const Weights neww) {
        for (int i = 0; i < NUMBINS; i++) bins[i] = newbins[i];
        for (int i = 0; i < BINSCAPACITY; i++) weights[i] = neww[i];
        minWeight = 1;
        maxWeight = BINSCAPACITY;
        nextPos = 0;
        failed = NONE;
    }

    __host__ __device__ Node(void) {
        for (int i = 0; i < NUMBINS; i++) bins[i] = 0;
        for (int i = 0; i < BINSCAPACITY; i++) weights[i] = 0;
        minWeight = 1;
        maxWeight = BINSCAPACITY;
        nextPos = 0;
        failed = NONE;
    }
    __host__ __device__ bool operator==(const Node &p) const {
        for (int i = 0; i < NUMBINS; i++) if (bins[i] != p.bins[i]) return false;
        return weights == p.weights;
    }

} Node;

typedef struct managedNodeVector {
    Node* data;
    Node ** storage;
    int numStored;
    int data_size;
    int cur_elts;

    managedNodeVector(void) {
        data = NULL;
        data_size = 0;
        cur_elts = 0;
    }
    managedNodeVector(int size) {
        cudaMallocManaged(&data, sizeof(Node) * size);
        storage = (Node**) malloc(100*sizeof(Node*));
        numStored = 0;
        data_size = size;
        cur_elts = 0;
    }
    ~managedNodeVector() {
        cudaFree(data);
        for (int i = 0; i < numStored; i++) {
            cudaFree(storage[i]);
        }
        free(storage);
    }
    inline Node& operator[](int i) {
        assert(i < data_size);
        cur_elts = max(i, cur_elts);
        return data[i];
    }
    inline Node operator[](int i) const {
        assert(i < cur_elts);
        return data[i];
    }
    void push_back(Node& node) {
        if (cur_elts >= data_size) {
            assert(numStored < 99);
            storage[numStored++] = data;
            cudaMallocManaged(&data, data_size * sizeof(Node));
            cur_elts = 0;
        }
        assert(cur_elts <= data_size - 1);
        data[cur_elts++] = node;
    }
} managedNodeVector;

typedef struct outputNode {
    Node node;
    enum status result;

    __device__ outputNode(Node newnode, enum status newresult) {
        node = newnode;
        result = newresult;
    }
} outputNode;


// specialized hash function for unordered_map keys
struct hash_fn
{
    size_t operator() (Weights w) const
	{
        size_t h = 0;
		for (int i = 0; i < BINSCAPACITY; i++) if (w[i] != 0) h ^= hash<char>()(w[i] * (i + 10) );
        return h;
    }
    size_t operator() (Node node) const
	{
        size_t h = 0;
        for (int i = 0; i < NUMBINS; i++) h ^= hash<char>()(node.bins[i]);
        h ^= operator()(node.weights);
		return h;
    }
    size_t operator() (pair<Node, char> pair) const
	{
		return operator()(get<0>(pair)) ^ hash<char>()(get<1>(pair));
    }
};

int globCycle;
typedef struct callTree {
    Node root; //the node that corresponds to the tree
    unordered_set<pair<Node, char>, hash_fn> parents; //parents 
    Node childrenLow[BINSCAPACITY]; //children
    Node childrenUpp[BINSCAPACITY];
    enum pos succeeded;
    enum pos failed;
    char nextWeightUpperBound;
    char nextWeightLowerBound;
    char nextPos;
    enum status status;
    int cycle;
    callTree(Node newroot) {
        root = {newroot.bins, newroot.weights};
        parents = {};
        failed = NONE;
        succeeded = NONE;
        nextPos = 0;
        cycle = 0;
        nextWeightLowerBound = 1;
        nextWeightUpperBound = BINSCAPACITY;
        status = DEFAULT;
    }
    callTree(void) {
        root = {};
        parents = {};
        failed = NONE;
        succeeded = NONE;
        nextPos = 0;
        cycle = 0;
        nextWeightLowerBound = 1;
        nextWeightUpperBound = BINSCAPACITY;
        status = DEFAULT;
    }
} callTree;

template<int X, int P>
struct Pow
{
    enum { result = X*Pow<X,P-1>::result };
};
template<int X>
struct Pow<X,0>
{
    enum { result = 1 };
};
template<int X>
struct Pow<X,1>
{
    enum { result = X };
};

typedef enum tribool {
    I,
    V, 
    F
} tribool;


__device__ __host__ void bubble(char (&bins)[NUMBINS], int i, char toAdd) {
    bins[i] += toAdd;
    while (i-1 >= 0 && bins[i-1]<bins[i]) {
        char temp = bins[i];
        bins[i] = bins[i-1];
        bins[i-1] = temp;
        i--;
    }
}

__device__ 
bool device_fit(Weights &weights) {
    char path[BINSCAPACITY*NUMBINS/2];
    char storedWeights[BINSCAPACITY] = {0};
    path[0] = -1;
    int act = 0; //current weight being fitted
    int sum = 0;
    char curWeight = BINSCAPACITY;
    char bins[NUMBINS] = {0}; //bins are maintained
    while(act >= 0){
        while (curWeight > 1 && weights[curWeight-1] == storedWeights[curWeight-1]) curWeight--;
        if (curWeight == 1) {
            return (sum + weights[0] <= NUMBINS*BINSCAPACITY);
        }
        int k = path[act] + 1;
        while (true) {
            if (k >= NUMBINS) { //backtrack if fitting isn't possible
                storedWeights[curWeight-1]--;
                while (storedWeights[curWeight-1] == 0) curWeight++;
                path[act] = -1;
                act--;
                if (act >= 0) bins[path[act]] -= curWeight;
                break;
            }
            else if (curWeight + bins[k] <= BINSCAPACITY) { //try to first fit
                bins[k] += curWeight;
                storedWeights[curWeight-1]++;
                path[act] = k;
                act++;
                path[act] = -1;
                break;
            }
            else k++;
        }
    }
    return false;
}

__global__
void main_kernel(Node* inputArray, int numInputs, outputNode* outputArray) {
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if (id >= numInputs) return;
    
    Node copy = inputArray[id];

    if (copy.failed != LOW) {
        copy.weights[copy.minWeight-1]++;
        bool fitted = device_fit(copy.weights);
        bubble(copy.bins, copy.nextPos, copy.minWeight);
        int mini = copy.bins[NUMBINS-1];
        int maxi = copy.bins[0];
        int sum = 0;
        for (int k = 0; k < NUMBINS; k++) sum += copy.bins[k];
        enum status res;
        if (not fitted) res = FAILURE;
        else if (maxi >= GOAL) res = SUCCESS;
        else if (mini + BINSCAPACITY*NUMBINS - sum < GOAL) res = FAILURE;
        else res = ONGOING;
        outputArray[2*id] = {copy, res};
    }

    copy = inputArray[id];
    if (copy.failed != HIGH) {
        copy.weights[copy.maxWeight-1]++;
        bool fitted = device_fit(copy.weights);
        bubble(copy.bins, copy.nextPos, copy.maxWeight);
        int mini = copy.bins[NUMBINS-1];
        int maxi = copy.bins[0];
        int sum = 0;
        for (int k = 0; k < NUMBINS; k++) sum += copy.bins[k];
        enum status res;
        if (not fitted) res = FAILURE;
        else if (maxi >= GOAL) res = SUCCESS;
        else if (mini + BINSCAPACITY*NUMBINS - sum < GOAL) res = FAILURE;
        else res = ONGOING;
        outputArray[2*id + 1] = {copy, res};
    }
}


static const int powers[7] = {Pow<BINSCAPACITY + 1, 0>::result, Pow<BINSCAPACITY + 1, 1>::result, Pow<BINSCAPACITY + 1, 2>::result, 
Pow<BINSCAPACITY + 1, 3>::result, Pow<BINSCAPACITY + 1, 4>::result, Pow<BINSCAPACITY + 1, 5>::result, Pow<BINSCAPACITY + 1, 6>::result};
struct fit_array {
    
    unordered_map<int, bool> array;

    fit_array(void) {
        array = {};
    }
    tribool get(const int (&bins)[NUMBINS], int y) {
        int indice = 0;
        for (int i = 0; i < NUMBINS; i++) {
            indice += bins[i] * powers[i];
        }
        indice += y * Pow<BINSCAPACITY + 1, NUMBINS>::result;
        auto ret = array.find(indice);
        if (ret == array.end()) return I;
        else return ret->second ? V : F;
    }
    bool& operator()(const int (&bins)[NUMBINS], int y) {
        int indice = 0;
        for (int i = 0; i < NUMBINS; i++) {
            indice += bins[i] * powers[i];
        }
        indice += y * Pow<BINSCAPACITY + 1, NUMBINS>::result;
        return array[indice];
    }
    void clear(void) {
        array.clear();
    }

};

unordered_map<Weights, bool, hash_fn> fit_map;
struct fit_array fit_array;

void bubble(int (&bins)[NUMBINS], int i, int toAdd) {
    bins[i] += toAdd;
    while (i-1 >= 0 && bins[i-1]<bins[i]) {
        int temp = bins[i];
        bins[i] = bins[i-1];
        bins[i-1] = temp;
        i--;
    }
}

bool fit_rec(Weights &weights, int (&bins)[NUMBINS], int numWeights, char curWeight, int sum) {
    tribool mem = fit_array.get(bins, numWeights);

    if (mem != I) return mem == V ? 1 : 0;

    while (curWeight > 0 && weights[curWeight-1] == 0) curWeight--;

    if (curWeight <= 0) return true;
    if (curWeight == 1) return BINSCAPACITY*NUMBINS - sum > weights[0];
    weights[curWeight-1]--;
    bool found = false;
    for (int i = 0; i < NUMBINS; i++) {
        int copy[NUMBINS] = {};
        for (int k = 0; k < NUMBINS; k++) copy[k] = bins[k];
        bubble(copy, i, curWeight);
        if (bins[i] + curWeight <= BINSCAPACITY) found = fit_rec(weights, copy, numWeights+1, curWeight, sum + curWeight);
        if (found) break;
    }
    weights[curWeight-1]++;
    fit_array(bins, numWeights) = found;
    return found;
}

bool fit(Weights weights) {
    auto prev = fit_map.find(weights);
    if (prev != fit_map.end()) return prev->second;
    else {
        fit_array.clear();
        int bins[NUMBINS] = {0};
        return fit_rec(weights, bins, 0, BINSCAPACITY, 0);
    }
}

void print_tree(const callTree &t) {
    printf("bins : [");
    for (int i = 0; i < NUMBINS; i++) {
        printf("%d ", t.root.bins[i]);
    }
    printf("] weights : [");
    for (int i = 0; i < BINSCAPACITY; i++) {
        printf("%d ", t.root.weights[i]);
    }
    printf("] failed : %s succeded : %s\n", t.failed == NONE ? "NONE" : (t.failed == HIGH ? "HIGH" : "LOW"), t.succeeded == NONE ? "NONE" : (t.succeeded == HIGH ? "HIGH" : "LOW"));
}
void propagateFailure(callTree& tree, unordered_map<Node, callTree, hash_fn> &cmap, managedNodeVector &toProcess) {
    if (ENABLE_PRINT) printf("pfail : ");
    if (ENABLE_PRINT) print_tree(tree);
    tree.status = FAILURE;
    for (pair<Node, char> pair : tree.parents) {
        Node parent = get<0>(pair);
        char prevWeight = get<1>(pair);
        auto& parentTree = cmap[parent];
        if (ENABLE_PRINT) printf("paren : ");
        if (ENABLE_PRINT) print_tree(parentTree);
        if (parentTree.status == FAILURE) continue; 
        if (parentTree.failed != NONE) {
            parentTree.nextWeightLowerBound++; //augmente et diminue les bornes pour essayer de nouveaux poids
            parentTree.nextWeightUpperBound--;
            parentTree.nextPos = 0;
            if (parentTree.nextWeightLowerBound > parentTree.nextWeightUpperBound) { //cela signifie que les deux poids ont déjà été essayés
                propagateFailure(parentTree, cmap, toProcess);
            }
            else  {
                if (parentTree.cycle < globCycle) {
                    parent.nextPos = parentTree.nextPos;
                    parent.minWeight = parentTree.nextWeightLowerBound;
                    parent.maxWeight = parentTree.nextWeightUpperBound;
                    parent.failed = parentTree.failed;
                    toProcess.push_back(parent);
                }
            }
            parentTree.cycle = globCycle;
        }
        else if (parentTree.succeeded != NONE && parentTree.cycle < globCycle) {
                parentTree.nextPos++;
                parentTree.failed = prevWeight <= parentTree.nextWeightLowerBound ? LOW : HIGH;
                parentTree.succeeded = NONE;
                parent.nextPos = parentTree.nextPos;
                parent.minWeight = parentTree.nextWeightLowerBound;
                parent.maxWeight = parentTree.nextWeightUpperBound;
                parentTree.cycle = globCycle;
                parent.failed = parentTree.failed;
                toProcess.push_back(parent);
        }
        else {
            parentTree.failed = prevWeight <= parentTree.nextWeightLowerBound ? LOW : HIGH;
        }
    }
    tree.parents.clear();
}

void propagateSuccess(callTree& tree, unordered_map<Node, callTree, hash_fn> &cmap, managedNodeVector &toProcess) {
    if (not fit(tree.root.weights)) {
        propagateFailure(tree, cmap, toProcess);
        return;
    }
    if (ENABLE_PRINT) printf("psucc : ");
    if (ENABLE_PRINT) print_tree(tree);
    tree.status = SUCCESS;
    for (pair<Node, char> pair : tree.parents) {
        Node parent = get<0>(pair);
        char prevWeight = get<1>(pair);
        auto& parentTree = cmap[parent];
        if (ENABLE_PRINT) printf("paren : ");
        if (ENABLE_PRINT) print_tree(parentTree);
        if (parentTree.status == SUCCESS) continue;   
        if (prevWeight <= parentTree.nextWeightLowerBound) {
            parentTree.childrenLow[parentTree.nextPos] = tree.root;
            if ((parentTree.succeeded == HIGH || parentTree.failed == HIGH) && parentTree.cycle < globCycle) {
                if (parentTree.nextPos >= NUMBINS - 1) propagateSuccess(parentTree, cmap, toProcess);
                else {
                    parentTree.nextPos++;
                    parentTree.succeeded = NONE;
                    parent.nextPos = parentTree.nextPos;
                    parent.minWeight = parentTree.nextWeightLowerBound;
                    parent.maxWeight = parentTree.nextWeightUpperBound;
                    parentTree.cycle = globCycle;
                    parent.failed = parentTree.failed;
                    toProcess.push_back(parent);
                }
            }
            else {
                parentTree.succeeded = LOW;
                if (parentTree.nextPos >= NUMBINS - 1) propagateSuccess(parentTree, cmap, toProcess);
            }
        }
        else if (prevWeight >= parentTree.nextWeightUpperBound) {
            parentTree.childrenUpp[parentTree.nextPos] = tree.root;
            if ((parentTree.succeeded == LOW || parentTree.failed == LOW) && parentTree.cycle < globCycle) {
                if (parentTree.nextPos >= NUMBINS - 1) propagateSuccess(parentTree, cmap, toProcess);
                else {
                    parentTree.nextPos++;
                    parentTree.succeeded = NONE;
                    parent.nextPos = parentTree.nextPos;
                    parent.minWeight = parentTree.nextWeightLowerBound;
                    parent.maxWeight = parentTree.nextWeightUpperBound;
                    parentTree.cycle = globCycle;
                    parent.failed = parentTree.failed;
                    toProcess.push_back(parent);
                }
            }
            else {
                parentTree.succeeded = HIGH;
                if (parentTree.nextPos >= NUMBINS - 1) propagateSuccess(parentTree, cmap, toProcess);
            }
        }
    }
    tree.parents.clear();
}

bool trackNode(Node &node, unordered_map<Node, callTree, hash_fn> &cmap, callTree &parentTree, char prevWeight, managedNodeVector &toProcess) {
    bool inserted = 
        cmap.emplace(std::piecewise_construct,
            forward_as_tuple(node),
            forward_as_tuple(node)).second; //insert an empty call tree node if node was not is the map.
    auto& t = cmap[node];
    if (ENABLE_PRINT) printf("track : ");
    if (ENABLE_PRINT) print_tree(t);
    t.parents.insert(make_pair(parentTree.root, prevWeight));
    if (t.status == SUCCESS) {
        propagateSuccess(t, cmap, toProcess);
    }
    else if (t.status == FAILURE) {
        propagateFailure(t, cmap, toProcess);
    }
    return inserted;
}
void generateNodes(Node &root, unordered_map<Node, callTree, hash_fn> &cmap, managedNodeVector &toProcess, int depth) {
    auto& tree = cmap[root];
    int mini = tree.root.bins[NUMBINS-1];
    int maxi = tree.root.bins[0];
    int sum = 0;
    for (int k = 0; k < NUMBINS; k++) sum += tree.root.bins[k];
    if (maxi >= GOAL) tree.status = SUCCESS;
    else if (mini + BINSCAPACITY*NUMBINS - sum < GOAL) tree.status = FAILURE;
    if (tree.status == FAILURE) {
        propagateFailure(tree, cmap, toProcess);
        return;
    }
    if (tree.status == SUCCESS) {
        propagateSuccess(tree, cmap, toProcess);
        return;
    }
    if (depth <= 0) {
        if (tree.cycle < globCycle) {
            root.nextPos = tree.nextPos;
            root.minWeight = tree.nextWeightLowerBound;
            root.maxWeight = tree.nextWeightUpperBound;
            tree.cycle = globCycle;
            root.failed = tree.failed;
            toProcess.push_back(root);
        }
    }
    else {
        assert (tree.nextPos < NUMBINS);
        //check the weight can be put in the bin, if not try the next bin, if not try the next weight
            for (int w = tree.nextWeightLowerBound; w <= tree.nextWeightUpperBound; w++) {
                for (int b = tree.nextPos; b < NUMBINS; b++) {
                    Node copy = root;
                    copy.weights[w-1]++;
                    bubble(copy.bins, b, tree.nextWeightLowerBound);
                    bool inserted = trackNode(copy, cmap, tree, tree.nextWeightLowerBound, toProcess);
                    if (inserted) generateNodes(copy, cmap, toProcess, depth - 1);
                }
            }
    }
}

void processOutput(outputNode* outputArray, Node* inputArray, int numInputs, managedNodeVector &toProcess, unordered_map<Node, callTree, hash_fn> &cmap) {
    for (int id = 0; id < numInputs; id++){
        Node input = inputArray[id];
        auto& inputTree = cmap[input];
        if (input.failed != LOW) {
            Node lowOutput = outputArray[2*id].node;
            enum status result = outputArray[2*id].result;
            inputTree.status = ONGOING;
            bool inserted = trackNode(lowOutput, cmap, inputTree, input.minWeight, toProcess);
            auto& lowOutputTree = cmap[lowOutput];
            if (result == FAILURE) {
                propagateFailure(lowOutputTree, cmap, toProcess);
            }
            else if (result == SUCCESS) {
                propagateSuccess(lowOutputTree, cmap, toProcess);
            }
            else if (inserted && lowOutputTree.cycle < globCycle) {
                lowOutputTree.cycle = globCycle;
                lowOutput.nextPos = lowOutputTree.nextPos;
                lowOutput.minWeight = lowOutputTree.nextWeightLowerBound;
                lowOutput.maxWeight = lowOutputTree.nextWeightUpperBound;
                lowOutput.failed = lowOutputTree.failed;
                toProcess.push_back(lowOutput);
            }
        }

        if (input.failed != HIGH) {
            Node upOutput = outputArray[2*id + 1].node;
            enum status result = outputArray[2*id + 1].result;
            bool inserted = trackNode(upOutput, cmap, inputTree, input.maxWeight, toProcess);
            auto& upOutputTree = cmap[upOutput];
            if (upOutputTree.cycle == globCycle);
            else if (result == FAILURE) {
                propagateFailure(upOutputTree, cmap, toProcess);
            }
            else if (result == SUCCESS) {
                propagateSuccess(upOutputTree, cmap, toProcess);
            }
            else if (inserted && upOutputTree.cycle < globCycle) {
                upOutputTree.cycle = globCycle;
                upOutput.nextPos = upOutputTree.nextPos;
                upOutput.minWeight = upOutputTree.nextWeightLowerBound;
                upOutput.maxWeight = upOutputTree.nextWeightUpperBound;
                upOutput.failed = upOutputTree.failed;
                toProcess.push_back(upOutput);
            }
        }
    }
}

void printTree_rec(FILE *f, callTree &t, unordered_map<Node, callTree, hash_fn> &cmap, int maxd) {
    if (maxd < 0 || t.succeeded == NONE) return;
    Node* array;
    if (t.succeeded == HIGH) array = t.childrenUpp;
    else array = t.childrenLow;
    for (int i = 0; i < NUMBINS; i++) {
        Node child = array[i];
        fprintf(f, "\" [ ");
        for (int i = 0; i < NUMBINS; i++) {
            fprintf(f, "%d, ", t.root.bins[i]);
        }
        fprintf(f, "] \" -> \" [ ");
        for (int i = 0; i < NUMBINS; i++) {
            fprintf(f, "%d, ", child.bins[i]);
        }
        fprintf(f, "] \";\n");
        printTree_rec(f, cmap[child], cmap, maxd-1);
    }
}
void printfTree(callTree &root, unordered_map<Node, callTree, hash_fn> &cmap) {
    auto file = fopen("tree.dot", "w");
    fprintf(file, "strict digraph A {\n");
    printTree_rec(file, root, cmap, 10);
    fprintf(file, "\n}");
}

int main() {
    globCycle = 1;
    unordered_map<Node, callTree, hash_fn> cmap;
    Node root = {};
    callTree initTree = {};
    cmap[root] = initTree;
    auto& rootTree = cmap[root];
    int blockSize = 1024;
    int numBlocks = 48;
    managedNodeVector toProcess = {blockSize*numBlocks};
    outputNode* outputArray;
    cudaMallocManaged(&outputArray, 2*blockSize*numBlocks*sizeof(outputNode));
    generateNodes(root, cmap, toProcess, 0);
    globCycle++;
    while(rootTree.status != FAILURE && rootTree.status != SUCCESS && (toProcess.cur_elts != 0 || toProcess.numStored != 0)) {
        if (globCycle < 20 || (globCycle >> 9) << 9 == globCycle) printf("sizes : %d %d %d %ld\n", globCycle, toProcess.numStored, toProcess.cur_elts, cmap.size());
        Node* inputArray;
        int numInputs;
        if (toProcess.numStored > 0) {
            inputArray = toProcess.storage[toProcess.numStored-1];
            numInputs = toProcess.data_size;
            toProcess.storage[toProcess.numStored-1] = NULL;
            toProcess.numStored--;
        }
        else {
            inputArray = toProcess.data;
            numInputs = toProcess.cur_elts;
            cudaMallocManaged(&toProcess.data, blockSize*numBlocks*sizeof(Node));
            toProcess.cur_elts = 0;
        }
        main_kernel<<<numBlocks, blockSize>>>(inputArray, numInputs, outputArray);
        cudaDeviceSynchronize();
        processOutput(outputArray, inputArray, numInputs, toProcess, cmap);
        cudaFree(inputArray);
        globCycle++;
    }
    printf("%s\n", rootTree.status == FAILURE ? "failure" : "success");
    printfTree(rootTree, cmap);
}