#include <iostream>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <cuda_profiler_api.h>
#include <cuda.h>

#define SORT(bins) tmp1 = bins[0];\
tmp2 = bins[1];\
tmp3 = bins[2];\
bins[0] = max(tmp1, max(tmp2, tmp3));\
bins[1] = min(tmp1, min(tmp2, tmp3));\
bins[1] = min(max(tmp1, tmp2), min(max(tmp2, tmp3), max(tmp1, tmp3))); //sort the bins

//-------------------------------Data structures and constants-------------------------------------
#define NUMBINS 3
#define BINSCAPACITY 14
#define GOAL 19

#define COPY(bins, newBins) for (int i = 0; i < NUMBINS; i++) {\
    newBins[i] = bins[i];\
    }

#define BANKSIZE (1024*1024)
#define HASHTABLECAPACITY (16 * 1024 * 1024) //number of entries in the hashtable
#define MAXVALUES (9*HASHTABLECAPACITY/10) //At this point no firther insertions must be made, because they would take too long.
#define HIGHCAPACITY (HASHTABLECAPACITY / 2) //At this point the program starts to delete values from the table.
#define KEMPTY (0xffffffff) //Empty constant
#define KDELETED (0xfffffffe)//deleted constant
#define KACCESSED (0xfffffffd) //when a key is being accessed

typedef struct node {
    char bins[NUMBINS];
    char nextLowerBound; //last low weight that has been tried 
    char nextUpperBound; //same for high weights
    char numWeights; //number of weights in the array
    uint16_t weightsHash; //hash for the weights, to get a fast hash computation
    char prevWeight; //Last weight that has been added
    char prevBin; //current value of last bin where a weight was added
    char* weights; //the weight array
} node;

const int queuesize = (128 * 95)/sizeof(node); //size of the shared queues
const int limitinf = queuesize/4; //When a queue reach this value it is refilled 5/8
const int limitsup = 5*queuesize/8; //When a queue reach this value it is emptied to 1/8
const int sizestep = queuesize/2;

typedef struct globalArrayList {
    node *head;
    globalArrayList *tail;
} globalArrayList;

typedef struct globalQueues {
    volatile int accessed; //if a thread is currently pushing in the queue//if a thread is fetching from the queue
    globalArrayList *first;
    globalArrayList *last;
} globalQueues;

enum status {
    QUEUED,
    IN_PROGRESS,
    SUCCESS,
    FAILURE,
};
typedef struct callTree {
    node root; //the node that corresponds to the tree
    callTree** parents; //parents 
    callTree* children[2*NUMBINS]; //children (only the successes, for the cpu to get it back at the end)
    char arrayMaxSize; //The parent array is extended when needed : this is the number of currently allocated slots
    char numParents; //and the number of slots actually in use
    bool deleted; //whether the tree was deleted
    bool failedOnce;
    char counterHigh;
    char counterLow; //number of successes for it
    enum status status;
} callTree;

typedef struct keyValue {
    uint32_t key; //hahshed key
    callTree value;
} keyValue;

typedef struct hashTbl {
    keyValue* values; 
    uint32_t* numValues;
} hashTbl;

typedef struct overflow {
    node* queue;
    uint32_t* len;
    uint32_t* start;
    node el;
    overflow* next;
} overflow;

typedef struct bank {
    uint32_t currentSize;
    bool curBank[BANKSIZE];
    struct bank* next;
} bank;
//-----------------------------auxilliary device functions---------------------------------------
//atomic add on char 

__device__ static inline char atomicAdd(char* address, char val) {
    size_t long_address_modulo = (size_t) address & 3;
    auto* base_address = (unsigned int*) ((char*) address - long_address_modulo);
    unsigned int long_val = (unsigned int) val << (8 * long_address_modulo);
    unsigned int long_old = atomicAdd(base_address, long_val);
    
    if (long_address_modulo == 3) {
        // the first 8 bits of long_val represent the char value,
        // hence the first 8 bits of long_old represent its previous value.
        return (char) (long_old >> 24);
    } 
    else {
        // bits that represent the char value within long_val
        unsigned int mask = 0x000000ff << (8 * long_address_modulo);
        unsigned int masked_old = long_old & mask;
        // isolate the bits that represent the char value within long_old, add the long_val to that,
        // then re-isolate by excluding bits that represent the char value
        unsigned int overflow = (masked_old + long_val) & ~mask;
        if (overflow) {
            atomicSub(base_address, overflow);
        }
        return (char) (masked_old >> 8 * long_address_modulo);
    }
}

__device__ static inline char atomicCAS(char* address, char expected, char desired) {
    size_t long_address_modulo = (size_t) address & 3;
    auto* base_address = (unsigned int*) ((char*) address - long_address_modulo);
    unsigned int selectors[] = {0x3214, 0x3240, 0x3410, 0x4210};

    unsigned int sel = selectors[long_address_modulo];
    unsigned int long_old, long_assumed, long_val, replacement;
    char old;

    long_val = (unsigned int) desired;
    long_old = *base_address;
    do {
        long_assumed = long_old;
        replacement = __byte_perm(long_old, long_val, sel);
        long_old = atomicCAS(base_address, long_assumed, replacement);
        old = (char) ((long_old >> (long_address_modulo * 8)) & 0x000000ff);
    } while (expected == old && long_assumed != long_old);

    return old;
}
//hashtable
__device__ //bin num specific
uint32_t hash(node node) { //murmur3 based hash function
    uint32_t hash = 0x509AD967;
    hash ^= node.bins[0] + (node.bins[1] << 8) + (node.bins[2] << 16);
    hash ^= hash >> 16;
    hash *= 0x9D5FA3CF;
    hash ^= node.weightsHash;
    hash ^= hash >> 16;
    hash *= 0x85ebca6b;
    hash ^= hash >> 13;
    hash *= 0xc2b2ae35;
    hash ^= hash >> 16;
    return hash & (HASHTABLECAPACITY-1);
}

__device__
uint16_t hashweight(char w) { //I did this without much thought, it is re hashed so it is not super important for it to be a good hash
    uint16_t w16 = (uint16_t) w;
    w16 ^= w16 >> 8;
    w16 *= 0x6D7F;
    w16 ^= w16 >> 5;
    w16 *= 0x955D;
    w16 ^= w16 >> 8;
    return w16;
    
}
__device__ //bin num dep
bool exactEqNodes(node n1, node n2) { //test the exact equality of two nodes, according lazily. Everything is sorted to prevent redundancy.
    if (n1.bins[0] != n2.bins[0]) return false;
    if (n1.bins[1] != n2.bins[1]) return false;
    if (n1.bins[2] != n2.bins[2]) return false;
    if (n1.weightsHash != n2.weightsHash) return false;
    if (n1.numWeights != n2.numWeights) return false;
    for (int i = 0; i < n1.numWeights; i++) {
        if (n1.weights[i] != n2.weights[i]) return false;
    }
    return true;
}

__device__ 
bool hashtbl_lookup(hashTbl hashtable, node unhashedKey, callTree* value) { //lookup a value in the table and copy it at address "value"
uint32_t slot = hash(unhashedKey);
while (true)
{
    if (hashtable.values[slot].key == slot && exactEqNodes(hashtable.values[slot].value.root, unhashedKey))
    {
        *value = hashtable.values[slot].value;
        return true;
    }
    if (hashtable.values[slot].key == KEMPTY || 
        (hashtable.values[slot].key == KDELETED && exactEqNodes(hashtable.values[slot].value.root, unhashedKey)))
        {
            return false;
        }
        slot = (slot + 1) & (HASHTABLECAPACITY - 1);
    }
}

__device__ 
bool hashtbl_lookup_ptr(hashTbl hashtable, node unhashedKey, callTree** value) { //lookup a value in the table and put a pointer to it at address "value"
uint32_t slot = hash(unhashedKey);
while (true)
{   
    if (hashtable.values[slot].key == slot && exactEqNodes(hashtable.values[slot].value.root, unhashedKey))
    {
        *value = &hashtable.values[slot].value;
        return true;
    }
    if (hashtable.values[slot].key == KEMPTY || 
        (hashtable.values[slot].key == KDELETED && exactEqNodes(hashtable.values[slot].value.root, unhashedKey)))
        {
            return false;
        }
        slot = (slot + 1) & (HASHTABLECAPACITY - 1);
    }
}

__device__ 
int hashtbl_insert_or_lookup(hashTbl hashtable, node unhashedKey, callTree value, callTree** ptr) { 
    //insert the value and put a pointer to it in ptr, of finds it and also put the pointer, whichever happens first
    uint32_t slot = hash(unhashedKey);
    uint32_t deb = slot;
    while (true)
    {
        uint32_t prev = atomicCAS(&hashtable.values[slot].key, KEMPTY, KACCESSED);
        if (prev == slot && exactEqNodes(hashtable.values[slot].value.root, unhashedKey)) {
            *ptr = &hashtable.values[slot].value;
            return 0;
        } 
        
        else if (prev == KEMPTY)
        {
            atomicAdd(hashtable.numValues, 1);
            hashtable.values[slot].value = value;
            *ptr = &hashtable.values[slot].value;
            hashtable.values[slot].key = slot;
            return 1;
        }
        else if (prev == KACCESSED) return 2; 
        else {
            uint32_t prev = atomicCAS(&hashtable.values[slot].key, KDELETED, KACCESSED);
            if (prev == KDELETED)
            {
                atomicAdd(hashtable.numValues, 1);
                hashtable.values[slot].value = value;
                *ptr = &hashtable.values[slot].value;
                hashtable.values[slot].key = slot;
                return 1;
            }
        }
        slot = (slot + 1) & (HASHTABLECAPACITY-1);
        if (slot - deb > 150) printf("PANIC %d\n", slot-deb);
    }
}


__device__ bool hashtbl_delete_ptr(hashTbl hashtable, node unhashedKey, callTree** value) { //delete a value form the table. It is flagged so it can be overwritten.
    uint32_t slot = hash(unhashedKey);
    while (true)
    {
        if (hashtable.values[slot].key == slot && exactEqNodes(hashtable.values[slot].value.root, unhashedKey))
        {
            atomicSub(hashtable.numValues, 1);
            *value = &hashtable.values[slot].value;
            hashtable.values[slot].key = KDELETED;
            return true;
        }
        if (hashtable.values[slot].key == KEMPTY || (hashtable.values[slot].key == KDELETED && exactEqNodes(hashtable.values[slot].value.root, unhashedKey)))
        {
            return false;
        }
        slot = (slot + 1) & (HASHTABLECAPACITY - 1);
    }
}

//Queue management
__device__
void pushToGlobalQueue(globalQueues* queueList, node* queue, uint32_t* queueStart, uint32_t* len) { //push half of a local queue to the global queue given in argument.
    while(atomicCAS((int*) &queueList->accessed, 0, 1) == 1); //Check if it is not being accessed. 
    node *array = (node*) malloc(sizestep * sizeof(node)); //allocate global memory for the queue
    int localCounter = (*queueStart - *len)%queuesize;
    for (int i = 0; i < sizestep; i++) { //copy the values
        if (localCounter >= queuesize) localCounter = 0;
        array[i] = queue[localCounter];
        localCounter++;
    }
    globalArrayList *newGlobArr;
    newGlobArr = (globalArrayList *) malloc(sizeof(globalArrayList));
    newGlobArr->head = array;
    newGlobArr->tail =  NULL;
    if (queueList->last == NULL) {
        queueList->last = newGlobArr;
        queueList->first = newGlobArr;
    }
    else {
        queueList->last->tail = newGlobArr;
        queueList->last = newGlobArr;
    }
    queueList->accessed = 0;
    
    *len = *len - sizestep; //updates the length
}

__device__
int fetchFromGlobalQueue(globalQueues* queueList, node* queue, uint32_t* queueStart, uint32_t* len) { //same, except it returns whether it managed to fetch something as the operation is not guaranteed.
    while(atomicCAS((int*) &queueList->accessed, 0, 1) == 1);
    globalArrayList *arrayList = queueList->first;
    if (arrayList == NULL) {
        queueList->accessed = false;
        return 0;
    }
    int localCounter = *queueStart;
    for (int i = 0; i < sizestep; i++) {
        if (localCounter >= queuesize) localCounter = 0;
        queue[localCounter] = arrayList->head[i];
        localCounter++;
    }
    queueList->first = arrayList->tail;
    if (arrayList->tail == NULL) {
        queueList->last = NULL;
    }
    free(arrayList->head);
    free(arrayList);
    *len = *len + sizestep;
    *queueStart = (*queueStart + sizestep)%queuesize;
    queueList->accessed = false;
    return 1;
}

__device__ //fetch one value from the local queue. Uses atomic operations to avoid conflicts.
bool fetchFromLocalQueue(node* queue, uint32_t* queueStart, uint32_t* len, node* ret_ptr) { 
    uint32_t old = *len;
    uint32_t prev;
    do {
        if (old == 0) return false;
        prev = old;
        old = atomicCAS(len, old, old - 1);
    } while (old != prev);
    
    *ret_ptr = queue[(*queueStart - old)%queuesize];
    return true;
}

__device__ //push one value to the local queue. Uses atomic operations to avoid conflicts.
bool pushToLocalQueue(overflow** overflowQueue, node* queue, uint32_t* queueStart, uint32_t* len, node el) { 
    uint32_t old = *len;
    uint32_t prev;
    do {

        if (old == queuesize-1) {
            overflow* ov = new(overflow);
            overflow* old = (overflow*) atomicExch((unsigned long long int*) overflowQueue, (unsigned long long int) ov);
            ov->queue = queue;
            ov->len = len;
            ov->start = queueStart;
            ov->next = old;
            ov->el = el;
            return true; 
        }
        prev = old;
        old = atomicCAS(len, prev, prev + 1);
    } while (old != prev);
    queue[atomicInc(queueStart, queuesize-1)] = el;
    return true;
}

//tree management
__device__
void addParent(callTree* tree, callTree* parent) { //add a parent to the parent list of a tree
    if (parent == NULL || parent->status != QUEUED) return;
    if (tree->arrayMaxSize == 0) { //if no memory was allocated yet, allocates two spaces. This should be enough most of the time.
        tree->arrayMaxSize = 2;
        tree->parents = (callTree**) malloc(2*sizeof(callTree*));
    }
    else if (tree->arrayMaxSize == tree->numParents) { //if sometimes it is not enough, doubles it.
        callTree** newArray = (callTree**) malloc(2*tree->arrayMaxSize*sizeof(callTree*));
        for (int i = 0; i < tree->arrayMaxSize; i++) {
            newArray[i] = tree->parents[i];
        }
        free(tree->parents); //free the precedent array
        tree->parents = newArray;
    }
    
    for (int i = 0; i < tree->numParents; i++) {
        if (exactEqNodes(tree->parents[i]->root, parent->root)) return; //if one of the previous parents is the same as the one being added, don't
    }
    int id = atomicAdd(&tree->numParents, (char) 1); //if not, do
    tree->parents[id] = parent;
}

//fit 
__device__ 
bool fit(char* weights, char numWeights) {
    char* path = (char*) malloc(sizeof(char)*numWeights);
    for (int i = 0; i < numWeights; i++) path[i] = -1;
    char act = 0; //current weight being fitted
    char bins[NUMBINS] = {0}; //bins are maintained
    while(act >= 0){
        if (act >= numWeights) {
            free(path);
            return true;
        }
        if (weights[act] == 1) {
            int sum = 0;
            for (int i = 0; i < NUMBINS; i++) {
                sum += bins[i];
            }
            return (sum + numWeights - act <= NUMBINS*BINSCAPACITY);
        }
        int k = path[act] + 1;
        while (true) {
            if (k >= NUMBINS) { //backtrack if fitting isn't possible
                path[act] = -1;
                act--;
                if (act >= 0) bins[path[act]] -= weights[act];
                break;
            }
            else if (weights[act] + bins[k] <= BINSCAPACITY) { //try to first fit
                bins[k] += weights[act];
                path[act] = k;
                act++;
                break;
            }
            else k++;
        }
    }
    free(path);
    return false;
}

__global__
void main_kernel(globalQueues* globalSuccessQueue, globalQueues* globalFailureQueue, globalQueues* globalToProcessQueue, globalQueues* globalProcessedQueue, hashTbl hashtable)
{
    int warpId = threadIdx.x/warpSize; //id of the warp of the current thread
    //-----------------------------------Memory allocation-----------------------------------------------------
    __shared__ node success[queuesize]; //queue to store nodes which are known to be successes
    __shared__ uint32_t successQueueStart;
    __shared__ uint32_t successQueueLen;
    
    __shared__ node failure[queuesize]; //queue to store nodes which are known to be failures
    __shared__ uint32_t failureQueueStart;
    __shared__ uint32_t failureQueueLen;
    
    __shared__ node toProcess[queuesize]; //queue to store node which have not yet been verified (but were fitted)
    __shared__ uint32_t toProcessQueueStart;
    __shared__ uint32_t toProcessQueueLen;
    
    __shared__ node processed[queuesize]; //queue to store nodes which are ready to be expanded
    __shared__ uint32_t processedQueueStart;
    __shared__ uint32_t processedQueueLen;
    
    __shared__ volatile bool exit; //whether the search has ended
    __shared__ volatile bool successStarved, failureStarved, toProcessStarved, processedStarved;
    __shared__ int lastPrint;
    __shared__ int exitCounter;
    __shared__ overflow* overflowQueue;
    //------------------------------------initialisation------------------------------------------------------
    if (threadIdx.x == 0) {
        exitCounter = 0;
        successQueueStart = 0;
        successQueueLen = 0;
        failureQueueStart = 0;
        failureQueueLen = 0; 
        toProcessQueueStart = 0;
        toProcessQueueLen = 0;
        processedQueueStart = 0;
        processedQueueLen = 0;
        overflowQueue = NULL;
        exit = false;
        if (blockIdx.x == 0) {
            node firstNode = {0};
            firstNode.nextLowerBound = 1;
            firstNode.nextUpperBound = BINSCAPACITY;
            pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, firstNode);
        }
    }
    __syncthreads();
    
    struct node node;
    struct node node2;
    bool gotNode;
    bool gotNode2;
    while (exitCounter < 50) {
        
        //------------------------------------------------cache management---------------------------------------------------------
        if (threadIdx.x == 0) {
            //printf("%d %d \n",blockIdx.x, *hashtable.numValues);
            if (successQueueLen > limitsup) //if there are too many elements, empty the queue (while other threads fetch from the queue)
            pushToGlobalQueue(globalSuccessQueue, success, &successQueueStart, &successQueueLen);
            if (successQueueLen < limitinf) { //if there are too little, do the reverse (while other threads are adding to the queue)
                successStarved = !fetchFromGlobalQueue(globalSuccessQueue, success, &successQueueStart, &successQueueLen);
            }
            else successStarved = false;
            //printf("block %d %d\n", blockIdx.x, *hashtable.numValues);
            if (*hashtable.numValues - lastPrint > 100) {
                printf("%d \n", *hashtable.numValues);
                lastPrint = *hashtable.numValues;
            }
        }
        else if (threadIdx.x == 1) {
            if (failureQueueLen > limitsup) 
            pushToGlobalQueue(globalFailureQueue, failure, &failureQueueStart, &failureQueueLen);
            if (failureQueueLen < limitinf) {
                failureStarved = !fetchFromGlobalQueue(globalFailureQueue, failure, &failureQueueStart, &failureQueueLen);
            }
            else failureStarved = false; 
        }
        else if (threadIdx.x == 2) {
            if (toProcessQueueLen > limitsup) 
            pushToGlobalQueue(globalToProcessQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen);
            if (toProcessQueueLen < limitinf) {
                toProcessStarved = !fetchFromGlobalQueue(globalToProcessQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen);
            }
            else toProcessStarved = false;
        }
        else if (threadIdx.x == 3) {
            if (processedQueueLen > limitsup) 
            pushToGlobalQueue(globalProcessedQueue, processed, &processedQueueStart, &processedQueueLen);
            
            if (processedQueueLen < limitinf) {
                processedStarved = !fetchFromGlobalQueue(globalProcessedQueue, processed, &processedQueueStart, &processedQueueLen);
            }
            else processedStarved = false;
        }
        
        
        __syncthreads();
        
        //-------------------------------------threads fetching what they need.------------------------------------------
        if (threadIdx.x == 4) {
            while (overflowQueue != NULL && *overflowQueue->len < limitsup) {
                if (*overflowQueue->len < limitsup) {
                    pushToLocalQueue(&overflowQueue, overflowQueue->queue, overflowQueue->start, overflowQueue->len, overflowQueue->el);
                    overflow* tmp = overflowQueue->next;
                    free(overflowQueue);
                    overflowQueue = tmp;
                }
            }
        }
        if (warpId == 0) {
            gotNode = fetchFromLocalQueue(success, &successQueueStart, &successQueueLen, &node);
        }
        else if (warpId == 1) {
            gotNode = fetchFromLocalQueue(failure, &failureQueueStart, &failureQueueLen, &node);
        }
        else if (warpId <= 4) {
            gotNode = fetchFromLocalQueue(toProcess, &toProcessQueueStart, &toProcessQueueLen, &node);
            gotNode2 = fetchFromLocalQueue(toProcess, &toProcessQueueStart, &toProcessQueueLen, &node2);
        }
        else {
            gotNode = fetchFromLocalQueue(processed, &processedQueueStart, &processedQueueLen, &node);
        }
        
        
        
        //the actual work being done
        __syncthreads();
        
        //--------------------------------------successes--------------------------------------------------------
        if (warpId == 0 && gotNode) {
            callTree* tree;
            if (hashtbl_lookup_ptr(hashtable, node, &tree) && (status) atomicCAS((char*) &tree->status, (char) QUEUED, (char) IN_PROGRESS) == QUEUED) //get a pointer to the corresponding tree
            {
                
                for (int i = 0; i < tree->numParents; i++) { //update the parents
                    callTree *parent = tree->parents[i];
                    char* addr;
                    if (tree->root.prevWeight == parent->root.nextLowerBound) addr = &parent->counterLow; //get the addresses in a variable to be able to update the value without warp divergence
                    else addr = &parent->counterHigh;
                    int numSuccesses = atomicAdd(addr, 1) + 1; //add 1 to the counter of how many successes there were. 
                    int id;
                    if (tree->root.prevWeight == parent->root.nextLowerBound) id = numSuccesses; //updates the children
                    else id = 2 * NUMBINS - 1 - numSuccesses;
                    
                    parent->children[id] = tree;
                    if (numSuccesses >= NUMBINS-1) { //if there are enough, then the parent is a success too
                        pushToLocalQueue(&overflowQueue, success, &successQueueStart, &successQueueLen, parent->root);
                    }
                }
                
                //free(tree->parents);
                tree->parents = NULL;
                tree->numParents = 0;
                tree->status = SUCCESS;
            }
        }
        //-------------------------------------failures----------------------------------------------
        else if (warpId == 1 && gotNode) {
            callTree* tree_ptr;
            callTree tree;
            bool found;
            found = hashtbl_lookup_ptr(hashtable, node, &tree_ptr); 
            if (found && (status) atomicCAS((char*) &tree_ptr->status, (char) QUEUED, (char) IN_PROGRESS) == QUEUED) {       

                tree  = *tree_ptr;
                
                for (int i = 0; i < tree.numParents; i++) {
                    callTree *parent = tree.parents[i];
                    int id1, id2, id3;
                    char* addr;
                    if (tree.root.prevWeight == parent->root.nextLowerBound) {
                        id1 = 0; //defines id to avoid thread divergence
                        id2 = 1;
                        id3 = 2;
                        addr = &parent->counterLow;
                        parent->root.nextLowerBound++;
                    }
                    else if (tree.root.prevWeight == parent->root.nextUpperBound) {
                        id1 = 3;
                        id2 = 4;
                        id3 = 5;
                        addr = &parent->counterHigh;
                        parent->root.nextUpperBound--;
                    }
                    else continue;
                    parent->children[id1] = NULL;
                    parent->children[id2] = NULL;
                    parent->children[id3] = NULL;
                    *addr = 0;
                    if (parent->root.nextUpperBound < parent->root.nextLowerBound) {
                        pushToLocalQueue(&overflowQueue, failure, &failureQueueStart, &failureQueueLen, parent->root);
                    }
                    else {
                        if (parent->failedOnce) { //uses the parent pointer to see if it is the second time that node failed, and if it can be processed again.
                            parent->failedOnce = false;

                            pushToLocalQueue(&overflowQueue, processed, &processedQueueStart, &processedQueueLen, parent->root);
                        }
                        else parent->failedOnce = true;
                    }
                    
                }
                
                tree_ptr->status = FAILURE;
                //free(tree_ptr->parents);
                tree_ptr->parents = NULL;
                tree_ptr->numParents = 0;
            }
            else pushToLocalQueue(&overflowQueue, failure, &failureQueueStart, &failureQueueLen, node);
        }
        //------------------------------------------------master threads-----------------------------------------------------
        else if (warpId <= 4 && warpId >= 2 && (gotNode || gotNode2)) {
            char tmp1, tmp2, tmp3;
            if (gotNode) {
                struct node parent = node;
                for (int i = 0; i < NUMBINS; i++) {
                    if (parent.bins[i] == parent.prevBin) {
                        parent.bins[i] -= parent.prevWeight;
                        SORT(parent.bins);
                        parent.numWeights--;
                        parent.weightsHash ^= hashweight(parent.prevWeight);
                        break;
                    }
                }
                callTree* parent_ptr;
                bool foundParent = hashtbl_lookup_ptr(hashtable, parent, &parent_ptr);
                callTree* tree_ptr;
                callTree tree;
                tree.root = node;
                tree.arrayMaxSize = 0;
                tree.parents = NULL;
                tree.numParents = 0;
                tree.status = QUEUED;
                tree.deleted = false;
                tree.failedOnce = false;
                int inserted = hashtbl_insert_or_lookup(hashtable, node, tree, &tree_ptr); //if the node is already there, use it. If not, insert it. 
                if (foundParent && parent_ptr->status != QUEUED);
                else if (inserted == 2 || tree_ptr->status == IN_PROGRESS) pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, node); //if in progress, delay
                else {
                    if (inserted) pushToLocalQueue(&overflowQueue, processed, &processedQueueStart, &processedQueueLen, node); //add it only if it is not in the hashtable. 
                    if (tree_ptr->status == SUCCESS)     pushToLocalQueue(&overflowQueue, success, &successQueueStart, &successQueueLen, node); //Re process the success if it is needed
                    else if (tree_ptr->status == FAILURE)     pushToLocalQueue(&overflowQueue, failure, &failureQueueStart, &failureQueueLen, node); //Same for failure.
                    else if (foundParent) {
                        if ((status) atomicCAS((char*) &tree_ptr->status, (char) QUEUED, (char) IN_PROGRESS) == QUEUED) {
                            addParent(tree_ptr, parent_ptr);
                            tree_ptr->status = QUEUED; 
                        }
                        else pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, node);
                    }//Anyway, add parent of the node as a parent.
                }
            }
            
            if (gotNode2) {
                struct node parent = node2;
                for (int i = 0; i < NUMBINS; i++) {
                    if (parent.bins[i] == parent.prevBin) {
                        parent.bins[i] -= parent.prevWeight;
                        SORT(parent.bins);
                        parent.numWeights--;
                        parent.weightsHash ^= hashweight(parent.prevWeight);
                        break;
                    }
                }
                callTree* parent_ptr;
                bool foundParent = hashtbl_lookup_ptr(hashtable, parent, &parent_ptr);
                callTree* tree_ptr;
                callTree tree;
                tree.root = node2; //do the same thing twice for better balancing.
                tree.arrayMaxSize = 0;
                tree.parents = NULL;
                tree.numParents = 0;
                tree.status = QUEUED;
                tree.deleted = false;
                tree.failedOnce = false;
                tree.counterHigh = 0;
                tree.counterLow = 0;
                int inserted = hashtbl_insert_or_lookup(hashtable, node2, tree, &tree_ptr); 
                if (foundParent && parent_ptr->status != QUEUED);
                else if (inserted == 2 || tree_ptr->status == IN_PROGRESS) pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, node2); 
                else {
                    if (inserted) pushToLocalQueue(&overflowQueue, processed, &processedQueueStart, &processedQueueLen, node2); //add it only if it is not in the hashtable. 
                    if (tree_ptr->status == SUCCESS)     pushToLocalQueue(&overflowQueue, success, &successQueueStart, &successQueueLen, node2); 
                    else if (tree_ptr->status == FAILURE)     pushToLocalQueue(&overflowQueue, failure, &failureQueueStart, &failureQueueLen, node2); 
                    else if (foundParent) {
                        if ((status) atomicCAS((char*) &tree_ptr->status, (char) QUEUED, (char) IN_PROGRESS) == QUEUED) {
                            addParent(tree_ptr, parent_ptr);
                            tree_ptr->status = QUEUED; 
                        }
                        else pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, node2);
                    }//Anyway, add parent of the node as a parent.
                }
            }
        }
        //------------------------------------------------worker threads-----------------------------------------------------
        else if (warpId >= 5 && gotNode){
            char tmp1, tmp2, tmp3;
            if (max(max(node.bins[0], node.bins[1]), node.bins[2]) >= GOAL) { //test if goal is attained
                pushToLocalQueue(&overflowQueue, success, &successQueueStart, &successQueueLen, node);
            }
            else if (min(min(node.bins[0], node.bins[1]), node.bins[2]) + 3*BINSCAPACITY - node.bins[0] - node.bins[1] - node.bins[2] < GOAL) { //or if success is not possible anymore
                pushToLocalQueue(&overflowQueue, failure, &failureQueueStart, &failureQueueLen, node);
            }
            else { //if none of that apply, go down in the tree
                //first process the lower weights
                struct node newNode = node;
                newNode.weightsHash ^= hashweight(node.nextLowerBound); //the new weight is hashed
                newNode.prevWeight = node.nextLowerBound;
                newNode.weights = (char*) malloc((node.numWeights + 1)*sizeof(char));
                newNode.nextLowerBound = 1;
                newNode.nextUpperBound = BINSCAPACITY;
                int k = 0;
                bool inserted = false;
                for (int i = 0; i <= node.numWeights; i++) { //copy the weights and sort them. That should probably be in some kind of cache fo better performance. 
                    if ((k >= node.numWeights || node.nextLowerBound >= node.weights[k]) && not inserted) {
                        newNode.weights[i] = node.nextLowerBound;
                        inserted = true;
                    }
                    else {
                        newNode.weights[i] = node.weights[k];
                        k++;
                    }
                }
                newNode.numWeights = node.numWeights + 1;
                if (fit(newNode.weights, newNode.numWeights)) { //if the weights fit, add all the weight placement to process queue. It might be smarter to only add the first, to do beta cuts.
                    newNode.bins[0] += node.nextLowerBound;
                    newNode.prevBin = newNode.bins[0];
                    SORT(newNode.bins); //uses the sort macro, for better efficiency, and lower thread divergence (it uses registers and max / min functions, which are parralelisable)
                    pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, newNode);
                    COPY(node.bins, newNode.bins)
                    newNode.bins[1] += node.nextLowerBound;
                    newNode.prevBin = newNode.bins[1];
                    SORT(newNode.bins);
                    pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, newNode);
                    COPY(node.bins, newNode.bins)
                    newNode.bins[2] += node.nextLowerBound;
                    newNode.prevBin = newNode.bins[2];
                    SORT(newNode.bins);
                    pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, newNode);


                    //same thing for upper weights, but if the lower bound didn't fit, the upper won't either, so don't even test it.
                    newNode = node;
                    newNode.weightsHash ^= hashweight(node.nextUpperBound);
                    newNode.prevWeight = node.nextUpperBound;
                    newNode.weights = (char*) malloc((node.numWeights + 1)*sizeof(char));
                    
                    k = 0;
                    inserted = false;
                    for (int i = 0; i <= node.numWeights; i++) {
                        if ((k >= node.numWeights ||node.nextUpperBound >= node.weights[k]) && not inserted) {
                            newNode.weights[i] = node.nextUpperBound;
                            inserted = true;
                        }
                        else {
                            newNode.weights[i] = node.weights[k];
                            k++;
                        }
                    }
                    newNode.numWeights = node.numWeights+1;
                    if (fit(newNode.weights, newNode.numWeights)) {
                        newNode.bins[0] += node.nextUpperBound;
                        newNode.prevBin = newNode.bins[0];
                        SORT(newNode.bins);
                        pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, newNode);
                        COPY(node.bins, newNode.bins)
                        newNode.bins[1] += node.nextUpperBound;
                        newNode.prevBin = newNode.bins[1];
                        SORT(newNode.bins);
                        pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, newNode);
                        COPY(node.bins, newNode.bins)
                        newNode.bins[2] += node.nextUpperBound;
                        newNode.prevBin = newNode.bins[2];
                        SORT(newNode.bins);
                        pushToLocalQueue(&overflowQueue, toProcess, &toProcessQueueStart, &toProcessQueueLen, newNode);
                    }
                }
                else pushToLocalQueue(&overflowQueue, failure, &failureQueueStart, &failureQueueLen, newNode);
            }
        }

        if (threadIdx.x == 0) {
            if (overflowQueue == NULL && successStarved && failureStarved && toProcessStarved && processedStarved && *hashtable.numValues > 20) {
            exitCounter++;
            }
            else exitCounter = 0;
        }

        if (threadIdx.x == 0) {
            successStarved = false;
            failureStarved = false;
            toProcessStarved = false;
            processedStarved = false;
        }
       __syncthreads();
    }
    if (threadIdx.x == 0) printf("Block %d exited at %d \n", blockIdx.x, *hashtable.numValues);
}

//-----------------------------------------Tests-------------------------------------------------------
/*__global__ 
void hashtable_test(hashTbl hashtable, int* verif) {
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    node n = {{id & 0xff, (id >> 8) & 0xff, (id >> 16) & 0xff}, 0};
    n.weights = (char*) malloc(1);
    n.weights[0] = id >> 24;
    n.numWeights = 1;
    n.weightsHash = id >> 24;
    callTree value = {n, NULL};
    value.arrayMaxSize = id & 0xffff;
    value.numParents = id >> 16;
    hashtbl_insert(hashtable, n, value);
    __syncthreads();
    if (not hashtbl_lookup(hashtable, n, &value)) verif[id] = -1;
    else if (value.arrayMaxSize != (id & 0xffff) || value.numParents != (id >> 16)) verif[id] = 1;
    else verif[id] = 0;
}

__global__ 
void test_kernel(hashTbl hashtable, int* verif){
    int id =  5*blockIdx.x * blockDim.x + threadIdx.x;
    for(int i = 0; i < 20; i++) {
        node n = {{id & 0xff, (id >> 8) & 0xff, (id >> 16) & 0xff}, 0};
        n.weights = (char*) malloc(8);
        n.weights[0] = (char) id >> 24;
        n.numWeights = (char) 1;
        n.weightsHash = (uint16_t) id >> 24;
        callTree value = {n, NULL, id};
        hashtbl_insert(hashtable, n, value);
        id++;
    }
}*/
//--------------------------------------host functions-------------------------------------------------
hashTbl create_hashtable() 
{
    // Allocate memory
    hashTbl hashtable;
    cudaMallocManaged(&hashtable.numValues, sizeof(int));
    cudaMallocManaged(&hashtable.values, sizeof(keyValue) * HASHTABLECAPACITY);
    // Initialize hash table to empty
    cudaMemset(hashtable.values, 0xff, sizeof(keyValue) * HASHTABLECAPACITY);
    *hashtable.numValues = 0;
    return hashtable;
}

int main(void)
{
    hashTbl hashtable = create_hashtable();
    globalQueues* globalSuccessQueue;
    globalQueues* globalFailureQueue;
    globalQueues* globalToProcessQueue;
    globalQueues* globalProcessedQueue;
    
    cudaMallocManaged(&globalSuccessQueue, sizeof(globalQueues));
    cudaMallocManaged(&globalFailureQueue, sizeof(globalQueues));
    cudaMallocManaged(&globalToProcessQueue, sizeof(globalQueues));
    cudaMallocManaged(&globalProcessedQueue, sizeof(globalQueues));
    
    *globalSuccessQueue = {0, NULL, NULL};
    *globalFailureQueue = {0, NULL, NULL};
    *globalToProcessQueue = {0, NULL, NULL};
    *globalProcessedQueue = {0, NULL, NULL};
    
    
    const size_t malloc_limit = 1024*1024*1024;
    cudaDeviceSetLimit(cudaLimitMallocHeapSize, malloc_limit);
    
    const int numBlocks = 24;
    const int blockSize = 256;
    main_kernel<<<numBlocks, blockSize>>>(globalSuccessQueue, globalFailureQueue, globalToProcessQueue, globalProcessedQueue, hashtable);
    //synctest<<<numBlocks, blockSize>>>();
    cudaDeviceSynchronize();
    printf("numValues : %d \n", *hashtable.numValues);
}