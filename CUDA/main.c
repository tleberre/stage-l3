#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <cassert>

using namespace std;
#define NUMBINS 6
#define BINSCAPACITY 14
#define GOAL 19

enum status {
    IDLE,
    ONGOING,
    FAILURE,
    SUCCESS,
    CANCELLED
};
typedef struct Weights {
    char array[BINSCAPACITY];

    Weights(void) {
        for (int i = 0; i < BINSCAPACITY; i++) array[i] = 0;
    }
    char& operator[](int i) {
        return array[i];
    }
    char operator[](int i) const {
        return array[i];
    }
    bool operator==(const Weights &p) const {
        bool ret = true;
        for (int i = 0; i < BINSCAPACITY; i++) ret &= array[i] == p.array[i];
        return ret;
    }
} Weights;

typedef struct Node {
    char bins[NUMBINS]; //the bins
    struct Weights weights; //the weight array
    char minWeight;
    char maxWeight;
    char nextPos;

    Node (const Node &original) {
        for (int i = 0; i < NUMBINS; i++) bins[i] = original.bins[i];
        for (int i = 0; i < BINSCAPACITY; i++) weights[i] = original.weights[i];
        minWeight = original.minWeight;
        maxWeight = original.maxWeight;
        nextPos = original.nextPos;
    }
    Node(const char newbins[NUMBINS], const Weights neww) {
        for (int i = 0; i < NUMBINS; i++) bins[i] = newbins[i];
        for (int i = 0; i < BINSCAPACITY; i++) weights[i] = neww[i];
        minWeight = 0;
        maxWeight = 0;
        nextPos = 0;
    }

    Node(void) {
        for (int i = 0; i < NUMBINS; i++) bins[i] = 0;
        for (int i = 0; i < BINSCAPACITY; i++) weights[i] = 0;
        minWeight = 0;
        maxWeight = 0;
        nextPos = 0;
    } 
    bool operator==(const Node &p) const {
        bool ret = true;
        for (int i = 0; i < NUMBINS; i++) ret &= bins[i] == p.bins[i];
        return ret && weights == p.weights;
    }

} Node;

// specialized hash function for unordered_map keys
struct hash_fn
{
    size_t operator() (Weights w) const
	{
        size_t h = 0;
		for (int i = 0; i < BINSCAPACITY; i++) if (w[i] != 0) h ^= hash<char>()(w[i] * (i + 10) );
        return h;
    }
    size_t operator() (Node node) const
	{
        size_t h = 0;
        for (int i = 0; i < NUMBINS; i++) h ^= hash<char>()(node.bins[i]);
        h ^= operator()(node.weights);
		return h;
    }
    size_t operator() (pair<Node, char> pair) const
	{
		return operator()(get<0>(pair)) ^ hash<char>()(get<1>(pair));
    }
};

typedef struct callTree {
    Node root; //the node that corresponds to the tree
    unordered_set<pair<Node, char>, hash_fn> parents; //parents 
    unordered_set<Node, hash_fn> children; //children
    bool failedOnce; //id of next bin to try
    char nextUpperBin;
    char nextWeightUpperBound;
    char nextWeightLowerBound;
    char nextLowerBin;
    enum status status;
    callTree(Node root) {
        root = {root.bins, root.weights};
        parents = {};
        children = {};
        failedOnce = 0;
        nextLowerBin = 0;
        nextUpperBin = 0;
        nextWeightLowerBound = 1;
        nextWeightUpperBound = BINSCAPACITY;
        status = IDLE;
    }
    callTree(void) {
        root = {};
        parents = {};
        children = {};
        failedOnce = 0;
        nextLowerBin = 0;
        nextUpperBin = 0;
        nextWeightLowerBound = 1;
        nextWeightUpperBound = BINSCAPACITY;
        status = IDLE;
    }
} callTree;

template<int X, int P>
struct Pow
{
    enum { result = X*Pow<X,P-1>::result };
};
template<int X>
struct Pow<X,0>
{
    enum { result = 1 };
};
template<int X>
struct Pow<X,1>
{
    enum { result = X };
};

typedef enum tribool {
    I,
    V, 
    F
} tribool;

static const int powers[7] = {Pow<BINSCAPACITY + 1, 0>::result, Pow<BINSCAPACITY + 1, 1>::result, Pow<BINSCAPACITY + 1, 2>::result, 
Pow<BINSCAPACITY + 1, 3>::result, Pow<BINSCAPACITY + 1, 4>::result, Pow<BINSCAPACITY + 1, 5>::result, Pow<BINSCAPACITY + 1, 6>::result};
struct fit_array {
    
    unordered_map<int, bool> array;

    fit_array(void) {
        array = {};
    }
    tribool get(const int (&bins)[NUMBINS], int y) {
        int indice = 0;
        for (int i = 0; i < NUMBINS; i++) {
            indice += bins[i] * powers[i];
        }
        indice += y * Pow<BINSCAPACITY + 1, NUMBINS>::result;
        auto ret = array.find(indice);
        if (ret == array.end()) return I;
        else return ret->second ? V : F;
    }
    bool& operator()(const int (&bins)[NUMBINS], int y) {
        int indice = 0;
        for (int i = 0; i < NUMBINS; i++) {
            indice += bins[i] * powers[i];
        }
        indice += y * Pow<BINSCAPACITY + 1, NUMBINS>::result;
        return array[indice];
    }
    void clear(void) {
        array.clear();
    }

};

unordered_map<Weights, bool, hash_fn> fit_map;
struct fit_array fit_array;

void bubble(int (&bins)[NUMBINS], int i, int toAdd) {
    bins[i] += toAdd;
    int j = i+1;
    while (bins[j]>bins[i] && j < NUMBINS) j++;
    char temp = bins[i];
    bins[i] = bins[j-1];
    bins[j-1] = temp;
}
void bubble(char (&bins)[NUMBINS], int i, char toAdd) {
    bins[i] += toAdd;
    int j = i+1;
    while (bins[j]>bins[i] && j < NUMBINS) j++;
    char temp = bins[i];
    bins[i] = bins[j-1];
    bins[j-1] = temp;
}

bool fit_rec(Weights &weights, int (&bins)[NUMBINS], int numWeights, char curWeight, int sum) {
    tribool mem = fit_array.get(bins, numWeights);

    if (mem != I) return mem == V ? 1 : 0;

    while (curWeight > 0 && weights[curWeight-1] == 0) curWeight--;

    if (curWeight <= 0) return true;
    if (curWeight == 1) return BINSCAPACITY*NUMBINS - sum > weights[0];
    weights[curWeight-1]--;
    bool found = false;
    for (int i = 0; i < NUMBINS; i++) {
        int copy[NUMBINS] = {};
        for (int k = 0; k < NUMBINS; k++) copy[k] = bins[k];
        bubble(copy, i, curWeight);
        if (bins[i] + curWeight <= BINSCAPACITY) found = fit_rec(weights, copy, numWeights+1, curWeight, sum + curWeight);
        if (found) break;
    }
    weights[curWeight-1]++;
    fit_array(bins, numWeights) = found;
    return found;
}

bool fit(Weights weights) {
    auto prev = fit_map.find(weights);
    if (prev != fit_map.end()) return prev->second;
    else {
        fit_array.clear();
        int bins[NUMBINS] = {0};
        return fit_rec(weights, bins, 0, BINSCAPACITY, 0);
    }
}

void cancel(callTree& tree, unordered_map<Node, callTree, hash_fn> &cmap) {
    for (Node child : tree.children) {
        auto& childTree = cmap[child];
        if (childTree.status != CANCELLED) {
            childTree.status = CANCELLED;
            cancel(childTree, cmap);
        }
    }
}
void propagateFailure(callTree& tree, unordered_map<Node, callTree, hash_fn> &cmap, vector<Node> &toProcess) {
    tree.status = FAILURE;
    cancel(tree, cmap);
    for (pair<Node, char> pair : tree.parents) {
        Node parent = get<0>(pair);
        auto& parentTree = cmap[parent];
        if (parentTree.status == FAILURE) continue; 
        if (parentTree.failedOnce) {
            parentTree.nextWeightLowerBound++; //augmente et diminue les bornes pour essayer de nouveaux poids
            parentTree.nextWeightUpperBound--;
            parentTree.status = IDLE;
            if (parentTree.nextWeightLowerBound > parentTree.nextWeightUpperBound) { //cela signifie que les deux poids ont déjà été essayés
                propagateFailure(parentTree, cmap, toProcess);
            }
            else toProcess.push_back(parent);
        }
        else parentTree.failedOnce = true;
    }
    tree.parents.clear();
}

void propagateSuccess(callTree& tree, unordered_map<Node, callTree, hash_fn> &cmap, vector<Node> &toProcess) {
    if (not fit(tree.root.weights)) {
        propagateFailure(tree, cmap, toProcess);
        return;
    }
    tree.status = SUCCESS;
    cancel(tree, cmap);
    for (pair<Node, char> pair : tree.parents) {
        Node parent = get<0>(pair);
        auto& parentTree = cmap[parent];
        if (parentTree.status == SUCCESS) continue;   
        char prevWeight = get<1>(pair);
        if (prevWeight <= parentTree.nextWeightLowerBound) {
            parentTree.nextLowerBin++;
            parentTree.status = IDLE;
            if (parentTree.nextLowerBin >= NUMBINS) propagateSuccess(parentTree, cmap, toProcess);
            else toProcess.push_back(parent);
        }
        else if (prevWeight >= parentTree.nextWeightUpperBound) {
            parentTree.nextUpperBin++;
            parentTree.status = IDLE;
            if (parentTree.nextUpperBin >= NUMBINS) propagateSuccess(parentTree, cmap, toProcess);
            else toProcess.push_back(parent);
        }
    }
    tree.parents.clear();
}

bool trackNode(Node &node, unordered_map<Node, callTree, hash_fn> &cmap, callTree &parentTree, char prevWeight) {
    bool inserted = 
        cmap.emplace(std::piecewise_construct,
            std::forward_as_tuple(node),
            std::forward_as_tuple(node)).second; //insert an empty call tree node if node was not is the map.
    cmap[node].parents.insert(make_pair(parentTree.root, prevWeight));
    parentTree.children.insert(node);
    return inserted;
}
void generateNodes(Node &root, vector<Node> &resultVector, unordered_map<Node, callTree, hash_fn> &cmap, vector<Node> &toProcess, int depth) {
    auto& tree = cmap[root];
    if (tree.status == FAILURE) {
        propagateFailure(tree, cmap, toProcess);
        return;
    }
    if (tree.status == SUCCESS) {
        propagateSuccess(tree, cmap, toProcess);
        return;
    }
    if (depth <= 0) {
        resultVector.push_back(root);
    }
    else {
        if (tree.nextLowerBin >= NUMBINS) {
            propagateSuccess(tree, cmap, toProcess);
            return;
        }
        //check the weight can be put in the bin, if not try the next bin, if not try the next weight
        while (tree.nextWeightLowerBound + root.bins[tree.nextLowerBin] >= GOAL && tree.nextWeightLowerBound <= tree.nextWeightUpperBound) { 
            tree.nextLowerBin++;
            if (tree.nextLowerBin >= NUMBINS) {
                propagateSuccess(tree, cmap, toProcess);
                return;
            }
        }
        if (tree.nextWeightLowerBound <= tree.nextWeightUpperBound) {
            Node copy = root;
            copy.weights[tree.nextWeightLowerBound-1]++;
            bubble(copy.bins, tree.nextLowerBin, tree.nextWeightLowerBound);
            trackNode(copy, cmap, tree, tree.nextWeightLowerBound);
            generateNodes(copy, resultVector, cmap, toProcess, depth - 1);
            tree.nextLowerBin++;
        }
        else {
            propagateFailure(tree, cmap, toProcess);
            return;
        }

        if (tree.nextUpperBin >= NUMBINS) {
            tree.nextUpperBin = 0;
            tree.nextWeightUpperBound++;
        }
        //check the weight can be put in the bin, if not try the next bin, if not try the next weight
        while (tree.nextWeightUpperBound + root.bins[tree.nextUpperBin] >= GOAL && tree.nextWeightLowerBound <= tree.nextWeightUpperBound) { 
            tree.nextUpperBin++;
            if (tree.nextUpperBin >= NUMBINS) {
                propagateSuccess(tree, cmap, toProcess);
                return;
            }
        }
        if (tree.nextWeightLowerBound <= tree.nextWeightUpperBound) {
            Node copy = root;
            copy.weights[tree.nextWeightUpperBound - 1]++;
            bubble(copy.bins, tree.nextUpperBin, tree.nextWeightUpperBound);
            trackNode(copy, cmap, tree, tree.nextWeightLowerBound);
            generateNodes(copy, resultVector, cmap, toProcess, depth - 1);
            tree.nextUpperBin++;
        }
        else {
            propagateFailure(tree, cmap, toProcess);
            return;
        }
    }
}


int main(int argc, char *argv[]) {
    unordered_map<Node, callTree, hash_fn> cmap;
    Node root = {};
    vector<Node> resultVector;
    vector<Node> toProcess;
    generateNodes(root, resultVector, cmap, toProcess, stoi(argv[1]));
    printf("sizes : %ld %ld %ld\n", resultVector.size(), toProcess.size(), cmap.size());
}