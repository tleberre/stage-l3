type graph = Node of int list * graph list
module SortedIntLists =
       struct
         type t = int list
         let rec compare l1 l2 =
           match l1, l2 with
           |[], [] -> 0
           |[], _ -> -1
           |_, [] -> 1
           |x1::q1, x2::q2 when Stdlib.compare x1 x2 = 0 -> compare q1 q2
           |x1::_, x2::_ -> Stdlib.compare x1 x2
       end
module ListSet = Set.Make(SortedIntLists)

let rec insert y l = match l with
  |[] -> [y]
  |x::q when x > y -> x::(insert y q)
  |x::q -> y::x::q
           
let rec make_1sums_list prec l = match l with
  |[] | [_] -> ListSet.empty
  |x::y::q -> ListSet.add ((insert (x+y) prec)@q) (make_1sums_list (prec@[x]) (y::q))
  
let make_graph n =
  let table = Hashtbl.create 150 in
  let rec parcours root_list =
    let ret = (Node
                 (root_list, List.map parcours
                    (List.of_seq
                       (ListSet.to_seq (make_1sums_list [] root_list))))) in
    Hashtbl.add table root_list ret;
    ret
  in
  let _ = parcours (List.init n (fun _ -> 1)) in
  table

let make_graph_array n =
  let a = Array.init n (fun x -> x+1) in
  Array.map make_graph a



