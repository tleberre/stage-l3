open Treedef

val c : int
val m : int

val calls : int ref
val lookups : int ref
val graphtime : float
val solve : int array -> int -> int list -> bool * tree
