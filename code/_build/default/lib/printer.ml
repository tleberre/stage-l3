open Treedef

let printtbl = Hashtbl.create 10000

let fprint_array fmt a =
  Format.fprintf fmt "[";
  Array.iter (Format.fprintf fmt "%d, ") a;
  Format.fprintf fmt "]"

let fprint_node fmt node = match node with
  |Tree (a, i, _) -> fprint_array fmt a ; Format.fprintf fmt "  --Next : %d" i
  |Leaf -> ()
  

let fprint_transition fmt q q' = if q' == Leaf || q == Leaf then () else
  Format.fprintf fmt "\"%a\" -> \"%a\";@\n"
    fprint_node q
    fprint_node q'

let rec fprint_tree_rec fmt tree =
  match tree with
  |Tree (a, _, l) ->
    if Hashtbl.mem printtbl a then ()
    else
      begin
        Hashtbl.add printtbl a 1; 
        List.iter
          (fun t' -> fprint_transition fmt tree t'; fprint_tree_rec fmt t')
          l
      end
  |Leaf -> ()

let fprint_tree fmt tree =
  Format.fprintf fmt "strict digraph A {@\n";
  fprint_tree_rec fmt tree;
  Format.fprintf fmt "@]@\n}@."
  
let save_tree file a =
  let ch = open_out file in
  Format.fprintf (Format.formatter_of_out_channel ch) "%a" fprint_tree a;
  close_out ch
