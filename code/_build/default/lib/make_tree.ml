open Treedef
open Partition_graphs
let c = 14

let m = 3

let nodes = 50000000
let table = Hashtbl.create nodes
let fit_table = Hashtbl.create 50000
let calls = ref 0
let lookups = ref 0 
let memcut = 20
let maxnodes = 500
let graphcut = 15
let incrcut = 15


let temp = Sys.time ()
let graphs = make_graph_array graphcut 
let graphtime = (Sys.time ()) -. temp

(*remet t.(i) à sa position triée dans un tableau*)
let rec pos t i =
  if i > 0 && t.(i-1) > t.(i)
  then
    begin
      let c = t.(i) in
      t.(i) <- t.(i-1);
      t.(i-1) <- c;
      pos t (i-1)
    end
  else
  if i + 1 < m && t.(i+1) < t.(i)
  then
    begin
      let c = t.(i) in
      t.(i) <- t.(i+1);
      t.(i+1) <- c;
      pos t (i+1)
    end

(*ajoute el à l'élément i d'un tableau et le remet à sa place*)
let add el i t = 
  let ret = Array.copy t in
  ret.(i) <- ret.(i) + el;
  pos ret i;
  ret

(*vérifie si une liste décroissante de poids rentre dans les boites (first fit avec backtracking*)
let first_fit seq =
  match Hashtbl.find_opt fit_table seq with
  |Some a -> a
  |None ->
  let rec first_fit_rec bins l k =
    match l with
    |[] -> true
    |x::q ->
      let rec fit i =
        if i >= m then -1
        else (
          if bins.(i) + x <= c then i
          else fit (i+1) )
      in
      let i = fit k in
      if i >= 0 then
        (first_fit_rec (add x i bins) q 0) || first_fit_rec bins l (i+1)
      else false
  in
  let ret = first_fit_rec (Array.make m 0) seq 0 in
  Hashtbl.replace fit_table seq ret;
  ret

(*insère un élément dans un eliste triée*)
let rec insert y l = match l with
  |[] -> [y]
  |x::q when x > y -> x::(insert y q)
  |x::q -> y::x::q

(*ajoute les combinaisons moins restrictive qu'une combinaison qui ne fonctionne pas au tableau*)
let add_bindings_false bins weights sum =
  if sum > graphcut || sum < 1 then ()
  else let gtable = graphs.(sum-1) in
    let num = ref 0 in
    let rec parcours (Node (il, gl)) =
      (incr num;
       Hashtbl.replace table (bins, il) (false, Leaf);
       if !num >= maxnodes then () else List.iter parcours gl )
    in
    parcours (Hashtbl.find gtable weights)

(*minimax qui considère les poids à ajouter en ordre décroissant*)
let rec solve_decr bins goal weights d =
  match Hashtbl.find_opt table (bins, weights) with (*on vérifie qu'on se connait pas déjà cette situation*)
  |Some a -> incr lookups; a
  |None -> incr calls;
    let sum = Array.fold_left (+) 0 bins in
    let maxValid = min c ((c*m) - sum) in
    let maxi = bins.(m-1) in
    let mini = bins.(0) in
    let x1, x3, alpha = bins.(2), bins.(0), goal - 1 - c in
    if sum >= c*m || maxi >= goal then (*le cas de la fin de la recherche*)
      (if  maxi >= goal then (true, Leaf)
       else (false, Leaf))
    else
    if mini + ((c*m) - sum) < goal || ((x1 >= 3*(c - alpha)/2 && x1 <= goal - 1) && x3 <= alpha) (*|| (x1+x2 >= 2*(c - alpha)/2 + x3/2 && x2 < c - 2*alpha && x3 < c - 2*alpha)*) then (*un cas où il n'est plus possible de dépasser le but*)
      (false, Leaf)
    else
      begin
        let found = ref false in
        let seq = ref [] in
        let toAdd = ref (maxValid + 1) in
        while !toAdd > 1 && not !found do
          decr toAdd;
          let w2 = (insert (!toAdd) weights) in (*la liste des poids*)
          if first_fit w2 then
            (let (bool, l) = solve_algo_decr bins !toAdd 0 goal w2 d in
             seq := l;
             found := bool)
        done;
        if d < memcut then
          (Hashtbl.add table (bins, weights) (!found, Tree (bins, !toAdd, !seq)));
        (!found, Tree (bins, !toAdd, !seq))
      end

and solve_algo_decr bins toAdd i goal weights d =
  if i >= m then (true, [])
  else
    let (bool, tree) = solve_decr (add toAdd i bins) goal weights d in
    if bool then
      begin
        let (boolcont, list) = solve_algo_decr bins toAdd (i+1) goal weights (d+1) in
        if boolcont then
          (true, tree::list)
        else
          (false, [])
      end
    else
      (false, [])

(*minimax qui considère les poids à ajouter en ordre croissant*)
let rec solve_incr bins goal weights d =
  match Hashtbl.find_opt table (bins, weights) with
  |Some a -> (*incr lookups;*) a
  |None -> (*incr calls;*)
    let sum = Array.fold_left (+) 0 bins in
    let maxValid = min c ((c*m) - sum) in
    let maxi = bins.(m-1) in
    (*let x2, x3, alpha = bins.(1), bins.(0), goal - 1 - c in*)
    if maxValid < c && maxi + maxValid < goal then
      (false, Leaf)
    else
      begin
        if sum >= c*m || maxi >= goal then
          (if  maxi >= goal then (true, Leaf)
           else (false, Leaf))
        else
        if ((maxi >= c - 2*(goal - c) && maxi <= goal - c - 1)) (*|| (weights <> [] && (List.hd weights) > alpha && x2 <= alpha && x2 >= (3*c - 7*alpha)/2 && x3 = 0)*) then
          (false, Leaf)
          else
          begin
            let found = ref false in
            let seq = ref [] in
            let toAdd = ref 0 in
            let tooBig = ref false in
            while !toAdd < maxValid && not !found && not !tooBig do
              incr toAdd;
              let w2 = (insert (!toAdd) weights) in
              if first_fit w2 then
                (let (bool, l) = if sum < incrcut
                   then solve_algo_incr bins !toAdd 0 goal w2 d
                   else solve_algo_decr bins !toAdd 0 goal w2 d in
                 seq := l;
                 found := bool)
              else
                tooBig := true
            done;
            if d < memcut then (
            (if (not !found) then add_bindings_false bins weights sum);
            Hashtbl.replace table (bins, weights) (!found, Tree (bins, !toAdd, !seq)));
            (!found, Tree (bins, !toAdd, !seq))
          end
      end

and solve_algo_incr bins toAdd i goal weights d =
  if i >= m then (true, [])
  else
    let (bool, tree) = solve_incr (add toAdd i bins) goal weights d in
    if bool then
      begin
        let (boolcont, list) = solve_algo_incr bins toAdd (i+1) goal weights (d+1) in
        if boolcont then
          (true, tree::list)
        else
          (false, [])
      end
    else
      (false, [])

(*on commence par faire de l'odre croissant, puis décroissant (dans l'idée, c'est une bonne stratégie de d'abord donner des petits poids puis des gros pour finir*)
let solve bins goal weights = solve_incr bins goal weights 0
