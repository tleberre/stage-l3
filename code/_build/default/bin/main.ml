open TreeUtils.Make_tree
open TreeUtils.Printer
open TreeUtils.Check_tree
open Printf

let goal = 19


let _ =
  let temptime = ref (Sys.time ()) in
  let (bool, tree) = solve (Array.make m 0) goal [] in
  let tot_time = (Sys.time ()) -. !temptime in
  temptime := Sys.time ();
  let check = check_tree tree m c goal in
  let check_time = (Sys.time ()) -. !temptime in
  temptime := Sys.time ();
  save_tree "tree.dot" tree;
  let save_time = (Sys.time ()) -. !temptime in
  if bool
  then
    begin
      printf "the program found a solution in %d calls and %d lookups. The check returned : %B\n" !calls !lookups check;
      printf "It took %f s to make the tree\n" tot_time;
      printf "It also took %f s to build the partition graph,  %f s to check the tree and %f s to save it\n" graphtime check_time save_time;
    end
  else
    printf "the program found no solution in %d calls\n" !calls;
