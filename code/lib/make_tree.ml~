 
open Treedef
open Partition_graphs
let c = 14

let m = 3

let nodes = 50000000
let table = Hashtbl.create nodes
let calls = ref 0
let lookups = ref 0 
let time = ref 0.
let hashtime = ref 0.
let tempTime = ref  0.

let maxnodes = 30
let graphcut = 16
let temp = Sys.time ()
let graphs = make_graph_array graphcut 
let graphtime = temp -. (Sys.time ())
let rec pos t i=
  if i > 0 && t.(i-1) > t.(i)
  then
    begin
      let c = t.(i) in
      t.(i) <- t.(i-1);
      t.(i-1) <- c;
      pos t (i-1)
    end
  else
  if i + 1 < m && t.(i+1) < t.(i)
  then
    begin
      let c = t.(i) in
      t.(i) <- t.(i+1);
      t.(i+1) <- c;
      pos t (i+1)
    end


let add el i t =
  let ret = Array.copy t in
  ret.(i) <- ret.(i) + el;
  pos ret i;
  ret

let first_fit seq =
  let rec first_fit_rec bins l k =
    match l with
    |[] -> true
    |x::q ->
      let rec fit i =
        if i >= m then -1
        else (
          if bins.(i) + x <= c then i
          else fit (i+1) )
      in
      let i = fit k in
      if i >= 0 then
        (first_fit_rec (add x i bins) q 0) || first_fit_rec bins l (i+1)
      else false
  in
  first_fit_rec (Array.make m 0) seq 0

let rec insert y l = match l with
  |[] -> [y]
  |x::q when x > y -> x::(insert y q)
  |x::q -> y::x::q

(*let rec test i k l weights = match weights with
  |x::y::_ when i = m-1 && x + y > c -> false
  |x::q -> if x > c/2 then test (i+1) i l q
    else if x == c/2 then test (i+1) k i q
    else test (i+1) k l q
  |[] -> 2*k + l <= 2*m*)
let add_bindings_false bins weights tree sum =
  if sum > graphcut then ()
  else let gtable = graphs.(sum-1) in
    let num = ref 0 in
    let rec parcours (Node (il, gl)) =
      (incr num;
       Hashtbl.add table (bins, il) (false, tree);
       if !num >= maxnodes then () else List.iter parcours gl )
    in
    parcours (Hashtbl.find gtable weights)

let rec solve bins goal weights =
  let first = (!time = 0.) in
  (if first then time := Sys.time ());
  tempTime := Sys.time ();
  let res = Hashtbl.find_opt table (bins, weights) in
  hashtime := !hashtime +. (Sys.time ()) -. !tempTime;
  match res with
  |Some a -> incr lookups; a
  |None -> incr calls;
    let sum = Array.fold_left (+) 0 bins in
    let maxValid = min c ((c*m) - sum) in
    let maxi = Array.fold_left max 0 bins in
    (if List.length weights <= -1 then Printf.printf "[ %s ] \n%!" (List.fold_left (^) "" (List.map (fun i -> " " ^ (string_of_int i) ^ " ") weights)));
    if sum >= c*m || maxi >= goal then
      (if  maxi >= goal then (true, Leaf)
       else (false, Leaf))
    else
      let found = ref false in
      let seq = ref [] in
      let toAdd = ref 0 in
      let tooBig = ref false in
      while !toAdd < maxValid && not !found && not !tooBig do
        incr toAdd;
        let w2 = (insert (!toAdd) weights) in
        if first_fit w2 then
        (let (bool, l) = solve_algo bins !toAdd 0 goal w2 in
        seq := l;
        found := bool)
        else
          tooBig := true
      done;
      (if (not !found) then add_bindings_false bins weights (Tree (bins, !toAdd, !seq)) sum);
      Hashtbl.add table (bins, weights) (!found, Tree (bins, !toAdd, !seq));
      (if first then time := Sys.time () -. !time);
      (!found, Tree (bins, !toAdd, !seq))

and solve_algo bins toAdd i goal weights =
  if i >= m then (true, [])
  else

    let (bool, tree) = solve (add toAdd i bins) goal weights in
    if bool then
      begin
        let (boolcont, list) = solve_algo bins toAdd (i+1) goal weights in
        if boolcont then
          (true, tree::list)
        else
          (false, [])
      end
    else
      (false, [])

