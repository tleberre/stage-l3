type graph = Node of int list * graph list

val make_graph_array : int -> (int list, graph) Hashtbl.t array
