open Treedef


let rec pos t i m =
  if i > 0 && t.(i-1) > t.(i)
  then
    begin
      let c = t.(i) in
      t.(i) <- t.(i-1);
      t.(i-1) <- c;
      pos t (i-1) m
    end
  else
  if i + 1 < m && t.(i+1) < t.(i)
  then
    begin
      let c = t.(i) in
      t.(i) <- t.(i+1);
      t.(i+1) <- c;
      pos t (i+1) m
    end

let add el i t m =
  let ret = Array.copy t in
  ret.(i) <- ret.(i) + el;
  pos ret i m;
  ret

let first_fit weights m c = let sorted_weights = List.sort (fun x y -> y-x) weights in
  let rec first_fit_rec bins l k =
    match l with
    |[] -> true
    |x::q ->
      let rec fit i =
        if i >= m then -1
        else (
          if bins.(i) + x <= c then i
          else fit (i+1) )
      in
      let i = fit k in
      if i >= 0 then
        (first_fit_rec (add x i bins m) q 0) || (first_fit_rec bins l (i+1))
      else false
  in
  first_fit_rec (Array.make m 0) sorted_weights 0

let rec check_tree_rec tree weights m c target_bins table goal = match tree with
  |Leaf -> if (Array.fold_left max 0 target_bins >= goal)
    then (if first_fit weights m c then true else (Printf.printf "[ %s ]\n" (List.fold_left (^) "" (List.map string_of_int weights)); false))
    else (Printf.printf "[ %s ]\n" (Array.fold_left (^) "" (Array.map string_of_int target_bins)); false)
  |Tree (bins, next, childs) ->
    try Hashtbl.find table (weights, bins)
    with Not_found ->
      if bins = target_bins
      then let bool = List.for_all (fun x -> x)
          (List.mapi
             (fun i tree -> check_tree_rec tree (next::weights) m c (add next i bins m) table goal )
             childs) in
        Hashtbl.add table (weights, bins) bool;
        bool
      else false 

  
let check_tree tree m c goal =
  let nodes = 50000000 in
  let table = Hashtbl.create nodes in
  check_tree_rec tree [] m c (Array.make m 0) table goal
